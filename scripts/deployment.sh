#!/bin/bash
# TODO Этот файл для развертывание проекта, но пока как задумка!!!
BASE_PATH=`dirname $0`


# Check if .env file exists.
if [ ! -e "$BASE_PATH/../.env" ]; then
    echo "ERROR: The .env file was not found or does not exist."
    exit 1
fi

# Check if docker is running.
if [[ $(docker info >/dev/null 2>&1) -ne 0 ]]; then
    echo "ERROR: Docker not working or not installed."
    exit 1
fi

# Check if docker-compose is install.
if [[ $(docker-compose --version >/dev/null 2>&1) -ne 0 ]]; then
    echo "ERROR: Docker-compose not installed."
    exit 1
fi

read_var() {
    VAR=$(grep $1 $2 | xargs)
    IFS="=" read -ra VAR <<< "$VAR"
    echo ${VAR[1]}
}

# Determine what type of assembly.
ENV_ENGINE=$(read_var ENV_ENGINE "$BASE_PATH/../.env")

case $ENV_ENGINE in
    production) DOCKER_COMPOSE="docker-compose -f $BASE_PATH/../docker-compose-prod.yml"
    ;;
    developmen) DOCKER_COMPOSE="docker-compose -f $BASE_PATH/../docker-compose-dev.yml"
    ;;
    *) echo "Invalid option for ENV_ENGINE = $ENV_ENGINE"
    ;;
esac


build_frontend(){
    $DOCKER_COMPOSE run --rm frontend sh -c "npm install --save && npm run build"
}

build_backend(){
    $DOCKER_COMPOSE build
}

# Очистка BackEnd`а.
clean_backend(){
    $DOCKER_COMPOSE run --rm backend sh -c "            \
        find . -type f -name '*.pyc' -delete &&         \
        find . -type f -name '*.pyc' -delete &&         \
        find . -type d -name '_pycache__' -delete &&    \
        rm -rf ./frontend/static &&                     \
        rm -rf ./frontend/templates                     " 
}

# Очистка FrontEnd`а.
clean_frontend(){
    $DOCKER_COMPOSE run --rm frontend sh -c 'rm -rf ./dist'
}

migrate(){
    $DOCKER_COMPOSE run --rm backend ./manage.py migrate --no-input
}

collectstatic(){
    $DOCKER_COMPOSE run --rm backend ./manage.py collectstatic --no-input
}

# Сборка всего проекта как BackEnd`а, так и FrontEnd`а.
build() {
    $DOCKER_COMPOSE build
    case $1 in
        backend)
            echo "  = Backend ="
            clean_backend
            build_backend
            collectstatic
        ;;
        frontend)
            echo "  = Frontend ="
            clean_frontend
            build_frontend
        ;;
        *)
            echo "  = Frontend ="
            clean
            build_frontend
            echo "  = Backend ="
            build_backend
            collectstatic
        ;;
    esac
}

# Запуск grandcore.org.
run(){
    $DOCKER_COMPOSE up -d
}

# Остановить grandcore.org.
stop(){
    $DOCKER_COMPOSE down -v
}

test(){
    echo "ERROR: Тесты не реализованны!"
    exit 1
    # Тест BackEnd`а.
    $DOCKER_COMPOSE run --rm backend sh -c " "

    # Тест FrontEnd`а.
    $DOCKER_COMPOSE run --rm frontend sh -c  "npm run test"
}

# Очистка BackEnd`а и FrontEnd`а.
clean(){
    case $1 in
        backend)
            echo "  = Backend ="
            clean_backend
        ;;
        frontend)
            echo "  = Frontend ="
            clean_frontend
        ;;
        *)
            echo "  = Backend ="
            clean_backend
            echo "  = Frontend ="
            clean_frontend
        ;;
    esac
}

# Запустить очистку Docker контейнеров.
dockerclean(){
    docker system prune -f
}

case $1 in
    build)
        echo "The build of the entire project as BackEnd and FrontEnd."
        build $2
    ;;
    run)
        echo "Run server grandcore.org."
        run
    ;;
    stop)
        echo "Stop server grandcore.org."
        stop
    ;;
    test)
        echo "Launching the testing project grandcore.org."
        test $2
    ;;
    clean)
        echo "Clean up assembly files:"
        clean $2
    ;;
    migrate)
        echo "Application of migrations, as well as for canceling the filing and listing of their status on the side of BackEnd."
        migrate
    ;;
    collectstatic)
        echo "Let's prepare / build static files."
        collectstatic
    ;;
    dockerclean)
        echo "Start cleaning Docker containers."
        dockerclean
    ;;
    *) echo "Invalid option: $1"
    ;;
esac

exit 0
